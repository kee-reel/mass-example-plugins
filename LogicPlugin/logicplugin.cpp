#include "logicplugin.h"

LogicPlugin::LogicPlugin(QObject* parent) :
	QObject(parent)
{
}

LogicPlugin::~LogicPlugin()
{
}

QString LogicPlugin::testString()
{
	return m_testString;
}

void LogicPlugin::setTestString(QString testString)
{
	if (m_testString == testString)
		return;
	
	m_testString = testString;
	qDebug() << "Test string changed:" << m_testString;
	emit testStringChanged(m_testString);
}
