#pragma once

#include <QtCore>

#include "../../Interfaces/Architecture/PluginBase/plugin_base.h"

#include "logicplugin.h"

class Plugin : public QObject, public PluginBase
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "MASS.Module.LogicPlugin" FILE "PluginMeta.json")
	Q_INTERFACES(IPlugin)
	
public:
	Plugin();
	~Plugin() override;
	
private:
	LogicPlugin* m_impl;
	// Commented code shows how to add new references. Uncomment, include reference interface and replace IExample with it.
	// After that add new entry to PluginMeta.json according to name of reference interface (don't forget version and keep valid JSON):
	// "references": {
	// 	"IHelloWorld/1.0": 1
	// },
	//ReferenceInstancePtr<IExample> m_exampleReference;
};
