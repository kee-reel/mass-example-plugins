#include "plugin.h"

Plugin::Plugin() :
	QObject(nullptr),
	PluginBase(this)
  , m_uiElementBase(new UIElementBase(this, {"MainMenuItem"}, "qrc:/form.qml"))
{
	initPluginBase(
				{
					{INTERFACE(IPlugin), this}
					, {INTERFACE(IUIElement), m_uiElementBase}
				},
				{
					{INTERFACE(ILogicPlugin), m_logicPlugin}
				}
				);
	
	m_uiElementBase->initUIElementBase(
		{{"logicPlugin", m_logicPlugin.data()}}
	);
}

Plugin::~Plugin()
{
}
